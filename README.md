# **JobHunter** <br/>

Frontend, Backend &amp; Webscrapper for JobHunter app

# **How to run the app** <br/>
## For Backend API
Run `Start Job Hunter Backend.bat` to start the backend API <br/>
or <br/>
Go to `target/classes/Root` and run `java Main`
## For Frontend
### Option 1 using executable file:
Go to `src/executable` and run `Job Hunter.exe`
### Option 2 using npm start:
Run `Start Job Hunter frontend.bat` <br/>
or <br/>
Go to `src/client` and from there run `npm install` and `npm start`


# **Endpoints**<br/>

endpoint: http://localhost:3000/search/:province/:profession/:jobSite <br/>

:province -> 0-20  <br/>
:profession -> string <br/>
:jobSite -> ALL, POSAO_HR, MOJPOSAO_HR <br/>

# **Login**
:email = asd@com.asd <br/>
:password = asd12 <br/>

ie.  http://localhost:3000/search/0/developer/ALL

# **Tools** <br/>

DB workbench: https://sqlitebrowser.org/ <br/>

# **External libs** <br/>

http://sparkjava.com/ <br/>
https://github.com/google/gson <br/>
https://www.sqlite.org/index.html <br/>
https://jsoup.org/ <br/>
http://www.slf4j.org/ <br/>
https://projectlombok.org/ <br/>

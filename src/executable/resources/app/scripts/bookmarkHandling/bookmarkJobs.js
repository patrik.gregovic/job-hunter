function bookmarkJob(email, employerName, joburl, location, expiresInEpoch, jobPosition) {

    let encodedJobUri = encodeURIComponent(joburl);
    let encodedEmployerUri = encodeURIComponent(employerName);
    let locationUri = encodeURIComponent(location);
    let positionUri = encodeURIComponent(jobPosition);

    console.log(`http://localhost:3000/jobs/${email}/${encodedEmployerUri}/${locationUri}/${encodedJobUri}/${locationUri}/${expiresInEpoch}/${positionUri}`)
    console.log(encodedJobUri);
    $.ajax({
        /*
            API url template:
            http://localhost:3000/jobs/:email/:employerName/:employerLocation/:jobUrl/:jobLocation/:jobExpires/:jobPosition
            NOTE: jobLocation = employerLocation
        */
        url: `http://localhost:3000/jobs/${email}/${encodedEmployerUri}/${locationUri}/${encodedJobUri}/${locationUri}/${expiresInEpoch}/${positionUri}`,
        type: 'POST',
        dataType: 'json', // added data type
        success: function (response) {
            if (response === true) {
                alert('Job successfuly added to bookmarks.');
                // localStorage.getItem("favJobs")
                //  localStorage
            } else {
                alert('Could not add job to bookmarks - try again later.');
            }
        }
    });
}

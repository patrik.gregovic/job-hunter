//on window load, get user information and load it into the html
$(document).ready(function () {
    getUserInformation();
    getCountries();
});

/**
 * Function for getting all suer information
 */
function getUserInformation() {

    var email = localStorage.getItem("email");

    $.ajax({
        /*
            API url template:
            http://localhost:3000/profile/:email/
        */
        url: `http://localhost:3000/profile/${email}`,
        type: 'GET',
        dataType: 'json', // added data type
        success: function (response) {
            var user = response;
            /*
                {
                    "id":"1",
                    "fName":"Brks",
                    "lName":"Brkselo",
                    "email":"mar@gmail.com",
                    "city":"Zagreb",
                    "country":"Croatia\n",
                    "streetName":"Plavo Brdo",
                    "streetNumber":"19"
                }
            */
            $('#fNameInfo').html(user.fName);
            $('#lNameInfo').html(user.lName);
            $('#emailInfo').html(user.email);
            $('#cityInfo').html(user.city);
            $('#countryInfo').html(user.country);
            $('#streetNameInfo').html(user.streetName);
            $('#streetNumInfo').html(user.streetNumber);
        }
    });
}

function getCountries() {
    $.ajax({
        /*
            API url template:
            http://localhost:3000/profile/:email/
        */
        url: `http://localhost:3000/countries`,
        type: 'GET',
        dataType: 'json', // added data type
        success: function (response) {
            var countries = response;
            /*
                [
                    {
                        "name":"Croatia",
                        "abv":"HR"
                    },
                    {
                        "name":"Albania",
                        "abv":"AL"
                    },
                    ....
                ]
            */
            var options = "";

            for (var i = 0; i < countries.length; i++) {
                options += `<option value="${countries[i].abv}">${countries[i].name}</option>`;
            }

            $('#country').html(options);

        }
    });
}

/**
 * onsubmit of the edit profile form, update user info
 */
$("#editForm").on("submit", function (e) {
    e.preventDefault();
    //get all values from the form
    var firstName = $('#firstName').val();
    var lastName = $('#lastName').val();
    var email = localStorage.getItem("email");
    var streetName = $('#streetName').val();
    var streetNum = $('#streetNum').val();
    var city = $('#city').val();
    console.log($('#country').val());
    var country = $('#country').val();
    var password = $('#password').val();
    var repeatedPassword = $('#repeatedPassword').val();

    if (repeatedPassword !== password) { //if passwords don't match - alert
        alert("Couldn't update user info - passwords didn't match! Please try again.");
        location.href = 'account.html';
    }

    $.ajax({
        /*
            API url template:
            http://localhost:3000/profile/:fname/:lname/:email/:streetName/:streetNumber/:city/:country/:password
        */
        url: `http://localhost:3000/profile/${firstName}/${lastName}/${email}/${streetName}/${streetNum}/${city}/${country}/${password}`,
        type: 'PUT',
        dataType: 'json', // added data type
        success: function (response) {
            if (response === true) {
                alert('Update successful!');
                location.href = 'index.html';
            }
            else {
                alert('Could not update user info - Please try again.')
                location.href = 'account.html';
            }
        }
    });
});
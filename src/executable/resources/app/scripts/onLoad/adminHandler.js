window.addEventListener('load', (e) => {
    e.preventDefault();
    getUsers();
});

function getUsers() {
    const email = localStorage.getItem("email")

    $.ajax({
        url: `http://localhost:3000/admin/getAll/${email}`,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            let getUserData = response;
            let userData = "";

            console.log(response);

            getUserData.forEach(el => {
                userData += `
                 <div class="job-result-card mb-4">
                    <button id="${el.email}" onClick="onDelete(this)" type="button" class="btn-close float-right"><i class="fas fa-times"></i><button>

                    <div class="card card-body shadow rounded">
                        <div class="row">
                                <div class="col-md-2 d-flex flex-column align-items-center">
                                    <img src="images/user.png" class="img-fluid rounded-circle company-logo" alt="Responsive image">
                                </div>

                                <div class="col-md-10 d-flex flex-column justify-content-center">
                                <h6>${el.fName} ${el.lName}</h6>
                                    <p><span class="location">Email:</span> ${el.email}</p>
                                    <p><span class="location">City:</span> ${el.city}</p>
                                    <p><span class="location">County:</span> ${el.county}</p>
                                    <p><span class="location">StreetName:</span> ${el.streetName}</p>
                                    <p><span class="location">StreetNumber:</span> ${el.streetNumber}</p>
                                    <p><span class="location">role:</span> ${el.role}</p>
                                </div>
                        </div>
                    </div>
                 </div>
                  `;
            });

            $("#userDataContainer").html(userData);
        }
    });
}

function onDelete(button) {
    $.ajax({
        url: `http://localhost:3000/admin/remove/${button.id}`,
        type: 'DELETE',
        dataType: 'json',
        success: function(response) {
            location.href = 'admin.html';
        },
        error: function(error) {
            console.error(error)
        }
    });
}

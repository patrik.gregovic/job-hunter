window.addEventListener('load', (e) => {
    e.preventDefault();
    getBookmarkedJobs();
});

function getBookmarkedJobs(isOnLogin) {
    $.ajax({
        url: `http://localhost:3000/jobs/${localStorage.getItem("email")}`,
        type: 'GET',
        dataType: 'json', // added data type
        success: function (response) {
            let fabJobs = response;

            if (isOnLogin) {
                localStorage.setItem('favJobs', JSON.stringify(fabJobs));
            }
            else createBookmarkCards(fabJobs);
        }
    });
}


function createBookmarkCards(fabJobs) {
    let job = "";
    fabJobs.forEach(el => {
        job += `
        <div class="job-result-card mb-4">
            <button id="${el.id}" onclick="onDelete(this)" type="button" class="btn-close float-right"><i class="fas fa-times"></i></button>
            
            <div class="card card-body shadow rounded">
                <div class="row">
                    <div class="col-md-10 d-flex flex-column justify-content-center">
                        <h6>${el.position}</h6>
                        <p class="mb-2">${el.employer}</p>
                        <p><span class="location">URL:</span> ${el.url}</p>
                        <p><span class="location">Expires:</span> ${new Date(el.expiresInEpoch).toLocaleDateString("en-US")}</p>
                        <p><span class="location">Location:</span> ${el.location}</p>
                    </div>
                </div>
            </div>
        </div>
        `;
    });

    $("#favContainer").html(job);
}


function onDelete(btn) {
    $.ajax({
        url: `http://localhost:3000/jobs/delete/${btn.id}`,
        type: 'DELETE',
        dataType: 'json', // added data type
        success: function (response) {
            getBookmarkedJobs(true);
            location.href = 'favorites.html';
            alert("Deleted Job From DB!")
        },
        error: function (error) {
            console.log("failed")
            console.error(error)
        }
    });

}

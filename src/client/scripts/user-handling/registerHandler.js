//on window load, get countries and load them into the countries dropdown
$(document).ready(function () {
    getCountries();
});


function getCountries() {
    $.ajax({
        /*
            API url template:
            http://localhost:3000/profile/:email/
        */
        url: `http://localhost:3000/countries`,
        type: 'GET',
        dataType: 'json', // added data type
        success: function (response) {
            var countries = response;
            /*
                [
                    {
                        "name":"Croatia",
                        "abv":"HR"
                    },
                    {
                        "name":"Albania",
                        "abv":"AL"
                    },
                    ....
                ]
            */
            var options = "";

            for (var i = 0; i < countries.length; i++) {
                options += `<option value="${countries[i].abv}">${countries[i].name}</option>`;
            }
            $('#country').html(options);

        }
    });
}


$("#registerForm").on("submit", function (e) {
    e.preventDefault();

    //get all values from the form
    var firstName = $('#firstName').val();
    var lastName = $('#lastName').val();
    var email = $('#email').val();
    var streetName = $('#streetName').val();
    var streetNum = $('#streetNum').val();
    var city = $('#city').val();
    var country = $('#country').val();
    var password = $('#password').val();
    var repeatedPassword = $('#repeatedPassword').val();

    if (repeatedPassword !== password) { //if passwords don't match - alert
        alert("Couldn't register user - passwords didn't match! Please try again.");
        location.href = 'register.html';
    }
    else { //if passwords match - create user
        $.ajax({
            /*
                API url template:
                http://localhost:3000/register/:fname/:lname/:email/:streetName/:streetNumber/:city/:country/:password
            */
            url: `http://localhost:3000/register/${firstName}/${lastName}/${email}/${streetName}/${streetNum}/${city}/${country}/${password}`,
            type: 'POST',
            dataType: 'json', // added data type
            success: function (response) {
                if (response === true) {
                    alert('Registration successful! Please log in.');
                    location.href = 'login.html';
                }
                else {
                    alert('Could not register user - Please try again.')
                    location.href = 'register.html';
                }
            }
        });
    }
});
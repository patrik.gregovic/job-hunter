//on window load, get user information and load it into the html
$(document).ready(function () {
    getUserInformation();
    getCountries();
});

document.getElementById("editUserBtn").addEventListener('click', updateInput);

/**
 * Function for getting all suer information
 */
function getUserInformation() {

    var email = localStorage.getItem("email");

    $.ajax({
        /*
            API url template:
            http://localhost:3000/profile/:email/
        */
        url: `http://localhost:3000/profile/${email}`,
        type: 'GET',
        dataType: 'json', // added data type
        success: function (response) {
            var user = response;
            /*
                {
                    "id":"1",
                    "fName":"Brks",
                    "lName":"Brkselo",
                    "email":"mar@gmail.com",
                    "city":"Zagreb",
                    "country":"Croatia\n",
                    "streetName":"Plavo Brdo",
                    "streetNumber":"19"
                }
            */
            $('#fNameInfo').html(user.fName);
            $('#lNameInfo').html(user.lName);
            $('#emailInfo').html(user.email);
            $('#cityInfo').html(user.city);
            $('#countryInfo').html(user.country);
            $('#streetNameInfo').html(user.streetName);
            $('#streetNumInfo').html(user.streetNumber);
        }
    });
}

function updateInput() {
    $('#firstName_input').val($('#fNameInfo').text());
    $('#lastName_input').val($('#lNameInfo').text());
    $('#email_input').val($('#emailInfo').text());
    $('#streetName_input').val($('#streetNameInfo').text());
    $('#streetNum_input').val($('#streetNumInfo').text());
    $('#city_input').val($('#cityInfo').text());
}

function getCountries() {
    $.ajax({
        /*
            API url template:
            http://localhost:3000/profile/:email/
        */
        url: `http://localhost:3000/countries`,
        type: 'GET',
        dataType: 'json', // added data type
        success: function (response) {
            var countries = response;
            /*
                [
                    {
                        "name":"Croatia",
                        "abv":"HR"
                    },
                    {
                        "name":"Albania",
                        "abv":"AL"
                    },
                    ....
                ]
            */
            var options = "";

            for (var i = 0; i < countries.length; i++) {
                options += `<option value="${countries[i].abv}">${countries[i].name}</option>`;
            }

            $('#country_input').html(options);

        }
    });
}

/**
 * onsubmit of the edit profile form, update user info
 */
$("#editForm").on("submit", function (e) {
    e.preventDefault();
    //get all values from the form
    var firstName = $('#firstName_input').val();
    var lastName = $('#lastName_input').val();
    var email = localStorage.getItem("email");
    var streetName = $('#streetName_input').val();
    var streetNum = $('#streetNum_input').val();
    var city = $('#city_input').val();
    var country = $('#country_input').val();
    var password = $('#password_input').val();
    var repeatedPassword = $('#repeatedPassword_input').val();

    if (repeatedPassword !== password) { //if passwords don't match - alert
        alert("Couldn't update user info - passwords didn't match! Please try again.");
        location.href = 'account.html';
    }

    $.ajax({
        /*
            API url template:
            http://localhost:3000/profile/:fname/:lname/:email/:streetName/:streetNumber/:city/:country/:password
        */
        url: `http://localhost:3000/profile/${firstName}/${lastName}/${email}/${streetName}/${streetNum}/${city}/${country}/${password}`,
        type: 'PUT',
        dataType: 'json', // added data type
        success: function (response) {
            if (response === true) {
                alert('Update successful!');
                location.href = 'index.html';
            }
            else {
                alert('Could not update user info - Please try again.')
                location.href = 'account.html';
            }
        }
    });
});

$("#loginForm").on("submit", function (e) {
    e.preventDefault();
    const email = $('#email').val();
    console.log(email)
    const password = $('#password').val();
    $.ajax({
        /*
            API url template:
            http://localhost:3000/login/:email/:password
        */
        url: `http://localhost:3000/login/${email}/${password}`,
        type: 'GET',
        dataType: 'json', // added data type
        success: async (response) => {
            if (response === true) {
                localStorage.setItem("users", "")
                localStorage.setItem("isAdmin", "false")
                localStorage.setItem("email", "")

                let isUserAdmin;
                isUserAdmin = await checkIfAdmin(email);

                if (isUserAdmin) {
                    let users = []
                    users = getAllUsers(email)

                    localStorage.setItem("users", JSON.stringify(users))
                    localStorage.setItem("isAdmin", "true");
                    localStorage.setItem("email", email);
                    location.href = 'admin.html';
                } else {
                    alert('Login successful!');
                    localStorage.setItem("email", email);
                    getBookmarkedJobs(true);
                    location.href = 'index.html';
                }

            }
            else {
                alert('Invalid credentials, try again.')
                location.href = 'login.html';
            }
        }
    });
});

const checkIfAdmin = (email) => {
    let isUserAdmin = false

    $.ajax({
        /*
            API url template:
            http://localhost:3000/login/:email
        */
        url: `http://localhost:3000/login/${email}`,
        async: false,
        type: 'GET',
        dataType: 'json', // added data type
        success: function (response) {
            isUserAdmin = response
        },
        error: (error) => {
            console.error(error)
        }
    });

    return isUserAdmin
}

const getAllUsers = () => {
    let users = []

    $.ajax({
        /*
            API url template:
            http://localhost:3000/admin/getAll/:email
        */
        url: `http://localhost:3000/admin/getAll/${email}`,
        async: false,
        type: 'GET',
        dataType: 'json', // added data type
        success: function (response) {
            users = response
        },
        error: (error) => {
            console.error(error)
        }
    });

    return users
}


function getBookmarkedJobs(isOnLogin) {
    $.ajax({
        url: `http://localhost:3000/jobs/${localStorage.getItem("email")}`,
        type: 'GET',
        dataType: 'json', // added data type
        success: function (response) {
            let fabJobs = response;
            let job = "";

            if (isOnLogin) {
                localStorage.setItem('favJobs', JSON.stringify(fabJobs));
            }
            else createBookmarkCards(fabJobs);
        }
    });
}

window.addEventListener('load', (e) => {
    e.preventDefault();
    getCounties(2);
});

const jobSiteSelect = document.getElementById("jobSiteSelect");

function getCounties(jobsite) {
    $.ajax({
        url: `http://localhost:3000/provinces/${jobsite}`,
        type: 'GET',
        dataType: 'json', // added data type
        success: function (response) {
            createOptions(response);
        }
    });
}

function createOptions(provinces) {
    const posaoSelect = document.getElementById("posaoSelect");

    let allOption = document.createElement("option");
    allOption.text = "All";
    allOption.value = "0";

    posaoSelect.add(allOption);

    provinces.forEach((el, index) => {
        let job = document.createElement("option");
        job.text = el.provinceCode;
        job.value = index + 1;

        posaoSelect.add(job);
    });
}

$("#searchForm").on("submit", function (e) {
    e.preventDefault();
    let province = $("#posaoSelect").val();
    let profession = document.getElementById("searchField").value;
    let jobsite = jobSiteSelect.options[jobSiteSelect.selectedIndex].value;

    $.ajax({
        url: `http://localhost:3000/search/${province}/${profession}/${jobsite}`,
        type: 'GET',
        dataType: 'json', // added data type
        success: function (response) {
            let jobPostings = response;
            let job = "";
            let email = localStorage.getItem("email");
            let favJobs = JSON.parse(localStorage.getItem("favJobs"));


            jobPostings.forEach((el, index) => {
                let btnSavedColor = "btn-save-green";
                favJobs.forEach(fav => {
                    console.log(fav.url);
                    console.log(el.url);
                    if (el.url === fav.url) {
                        btnSavedColor = "btn-save-gray";
                    }
                });
                job += `
                <div class="job-result-card mb-4">
                    <button type="button" class="btn-save float-right ${btnSavedColor}" id="addBookmark${index}"><i class="far fa-heart"></i></button>
                    <div class="card card-body shadow rounded">
                        <div class="row">
                            <div class="col-md-12 d-flex flex-column justify-content-center">
                                <h6>${el.position}</h6>
                                <p class="mb-2">Employer: ${el.employer}</p>
                                <p><span class="location">Location:</span>  ${el.location}</p>
                                <p><span class="location">URL:</span>  ${el.url}</p>
                                <p><span class="location">Expires:</span>  ${new Date(el.expiresInEpoch * 1000).toLocaleDateString("en-US")}</p>
                            </div>
                        </div>

                    </div>
                </div>
                `;
            });


            $("#jobPostings").html(job);
            jobPostings.forEach((el, index) => {
                $(`#addBookmark${index}`).click(function () {
                    if ($(`#addBookmark${index}`).hasClass("btn-save-green"))
                        bookmarkJob(`#addBookmark${index}`, email, el.employer, el.url, el.location, el.expiresInEpoch, el.position);
                });
            });
        }
    });
});


function bookmarkJob(buttonId, email, employerName, joburl, location, expiresInEpoch, jobPosition) {

    let encodedJobUri = encodeURIComponent(joburl);
    let encodedEmployerUri = encodeURIComponent(employerName);
    let locationUri = encodeURIComponent(location);
    let positionUri = encodeURIComponent(jobPosition);

    console.log(`http://localhost:3000/jobs/${email}/${encodedEmployerUri}/${locationUri}/${encodedJobUri}/${locationUri}/${expiresInEpoch}/${positionUri}`)
    console.log(encodedJobUri);
    $.ajax({
        /*
            API url template:
            http://localhost:3000/jobs/:email/:employerName/:employerLocation/:jobUrl/:jobLocation/:jobExpires/:jobPosition
            NOTE: jobLocation = employerLocation
        */
        url: `http://localhost:3000/jobs/${email}/${encodedEmployerUri}/${locationUri}/${encodedJobUri}/${locationUri}/${expiresInEpoch}/${positionUri}`,
        type: 'POST',
        dataType: 'json', // added data type
        success: function (response) {
            if (response === true) {
                $(buttonId).removeClass('btn-save-green');
                $(buttonId).addClass('btn-save-gray');
                $(buttonId).attr('disabled', 'true');
                alert('Job successfuly added to bookmarks.');
                getBookmarkedJobs(true);
            } else {
                alert('Could not add job to bookmarks - try again later.');
            }
        }
    });
}


function getBookmarkedJobs(isOnLogin) {
    $.ajax({
        url: `http://localhost:3000/jobs/${localStorage.getItem("email")}`,
        type: 'GET',
        dataType: 'json', // added data type
        success: function (response) {
            let fabJobs = response;

            if (isOnLogin) {
                localStorage.setItem('favJobs', JSON.stringify(fabJobs));
            }
            else createBookmarkCards(fabJobs);
        }
    });
}
//on window load, get user information and load it into the html
$(document).ready(function () {
    shouldDisplayAdminIcon()
});

const shouldDisplayAdminIcon = () => {
    let users = localStorage.getItem("users")

    if(!users) {
        $("#admin").hide()
    }
}

package Root;

import Root.db.DatabaseConnectivity;
import com.google.gson.Gson;
import Root.scrapper.JobService;

public class Resources {
    private static Gson gson;
    private static JobService jobService;
    private static DatabaseConnectivity dbConn;

    private Resources() {}

    public static Gson getGson() {
        if (gson == null) {
            return gson = new Gson();
        }
        return gson;
    }

    public static JobService getJobService() {
        if (jobService == null) {
            return jobService = new JobService();
        }
        return jobService;
    }

    public static DatabaseConnectivity getDB() {
        if(dbConn == null) {
            dbConn = new DatabaseConnectivity();
            dbConn.connect("jdbc:sqlite:jobHunter.db");
        }

        return dbConn;
    }
}

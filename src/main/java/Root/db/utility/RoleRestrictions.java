package Root.db.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RoleRestrictions extends HashMap<String, List<String>> {

    public RoleRestrictions() {
        this.put("Admin", new ArrayList<String>(){{
            add("Delete User");
        }});

        this.put("User", new ArrayList<String>(){{
            add("Bookmark CRUD");
            add("Search Jobs");
        }});
    }
}

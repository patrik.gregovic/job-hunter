package Root.db;

import lombok.Getter;
import lombok.Setter;

import java.sql.*;
import java.util.List;


public class DatabaseConnectivity {
    private @Getter
    Connection conn;
    private @Getter
    @Setter
    String uri;

    public boolean connect(String uri) {
        try {
            this.conn = DriverManager.getConnection(uri);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return this.conn != null;
        }
    }

    public ResultSet getData(String query) {
        ResultSet rs = null;
        try {
            rs = this.conn.createStatement().executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

    public int setData(String sqlStatement, List<String> values) {
        int success = -1;

        try {
            PreparedStatement preparedStatement = this.prepare(sqlStatement, values);
            success = preparedStatement.executeUpdate();
        } catch (Exception e) { e.printStackTrace(); }

        return success;
    }

    public int setData(String sqlStatement, List<String> values, boolean shouldGetKey) {
        ResultSet generatedKeyRS = null;
        int generatedKey = -1;

        try {
            PreparedStatement preparedStatement = this.prepare(sqlStatement, values);
            preparedStatement.executeUpdate();
            generatedKeyRS = preparedStatement.getGeneratedKeys(); // Get key from created row
        } catch (Exception e) { e.printStackTrace(); }

        try {
            generatedKey = generatedKeyRS.getInt(1);
        } catch (SQLException error) {
            error.printStackTrace();
        }

        return generatedKey;
    }

    public PreparedStatement prepare(String sqlStatement, List<String> values) {
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = this.conn.prepareStatement(sqlStatement);
            for (int i = 0; i < values.size(); i++) {
                preparedStatement.setObject(i + 1, values.get(i));
            }
        } catch (Exception e) { e.printStackTrace(); }

        return preparedStatement;
    }

    public PreparedStatement prepare(String sqlStatement, List<String> values, String[] generated) {
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = this.conn.prepareStatement(sqlStatement, generated);
            for (int i = 0; i < values.size(); i++) {
                preparedStatement.setObject(i + 1, values.get(i));
            }
        } catch (Exception e) { e.printStackTrace(); }

        return preparedStatement;
    }

    public int removeData(String query) {
        int removedRowId = -1;

        try {
            removedRowId = this.conn.createStatement().executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return removedRowId;
    }
}

package Root.db.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@AllArgsConstructor
public class PreferredSearch {
    private @NonNull @Getter final int id;
    private @NonNull @Getter @Setter String jobType;
    private @NonNull @Getter @Setter String province;
    private @NonNull @Getter @Setter int userId;
}

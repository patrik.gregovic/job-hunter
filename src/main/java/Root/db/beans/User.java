package Root.db.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
public class User {
    private @Getter @Setter String id;
    private @Getter @Setter String fName;
    private @Getter @Setter String lName;
    private @Getter @Setter String email;
    private @Getter @Setter String password;
    private @Getter @Setter String city;
    private @Getter @Setter String country;
    private @Getter @Setter String streetName;
    private @Getter @Setter String streetNumber;
    private @Getter @Setter String role;
    private @Getter @Setter String session;
    private @Getter @Setter String preferredSearch;

    public User(String id, String fName, String lName, String email, String city, String country, String streetName, String streetNumber, String role) {
        this.id = id;
        this.fName = fName;
        this.lName = lName;
        this.email = email;
        this.city = city;
        this.country = country;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.role = role;
    }
}

package Root.db.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Province {
    private @Getter @Setter String provinceCode;
}

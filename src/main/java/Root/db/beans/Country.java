package Root.db.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Country {
    private @Getter @Setter String name;
    private @Getter @Setter String abv;
}

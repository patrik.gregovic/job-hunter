package Root.db.beans;

import lombok.Getter;
import lombok.Setter;

public class JobPosting {
    private @Getter int id;
    private @Getter @Setter String url;
    private @Getter @Setter String position;
    private @Getter @Setter String location;
    private @Getter @Setter long expiresInEpoch;
    private @Getter @Setter String employer;

    public JobPosting(int id, String url, String position, String location, long expiresInEpoch, String employer) {
        this.id = id;
        this.url = url;
        this.position = position;
        this.location = location;
        this.expiresInEpoch = expiresInEpoch;
        this.employer = employer;
    }

    public JobPosting(String url, String position, String location, long expiresInEpoch, String employer) {
        this.url = url;
        this.position = position;
        this.location = location;
        this.expiresInEpoch = expiresInEpoch;
        this.employer = employer;
    }
}

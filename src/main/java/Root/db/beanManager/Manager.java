package Root.db.beanManager;

import Root.Resources;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class Manager {

    protected boolean authenticate(String id, String password) {
        ResultSet rs = Resources.getDB().getData("SELECT * FROM user;");

        try {
            while (rs.next()) {
                if (rs.getString("user_email").equals(id) &&
                        rs.getString("user_password").equals(password)) {
                    return true;
                }
            }
        } catch (SQLException e) { e.printStackTrace(); }

        return false;
    }

    protected abstract <T> T post(String query);

    protected abstract <T> T put(String query);

    protected abstract <T> T fetch(String query);

    protected abstract <T> T remove(String query);
}

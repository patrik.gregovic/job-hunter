package Root.db.beanManager;

import Root.Resources;
import Root.db.beans.User;
import spark.Request;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AdminManager extends Manager {

    @Override
    protected <T> T post(String query) {
        return null;
    }

    @Override
    protected <T> T put(String query) {
        return null;
    }

    @Override
    protected <T> T fetch(String query) {
        return null;
    }

    @Override
    protected <T> T remove(String query) {
        return null;
    }

    public List<User> getAllUsers(Request request) {
        List<User> users = new ArrayList<>();
        ResultSet userRs = Resources.getDB().getData("SELECT * FROM user WHERE user_email != " + "\"" + request.params("email") + "\"");

        try {
            while (userRs.next()) {
                ResultSet addressRs = Resources.getDB().getData("SELECT * FROM address WHERE address_id=" + "\"" + userRs.getString("user_address_id_fk") + "\"");
                ResultSet countryRs = Resources.getDB().getData("SELECT country_name FROM country WHERE country_id=" + "\"" + addressRs.getString("address_country_id_fk") + "\"");
                ResultSet roleRs = Resources.getDB().getData("SELECT role_name FROM role WHERE role_id=" + "\"" + userRs.getString("user_role") + "\"");

                users.add(new User(
                        userRs.getString("user_id"),
                        userRs.getString("user_first_name"),
                        userRs.getString("user_last_name"),
                        userRs.getString("user_email"),
                        addressRs.getString("address_city"),
                        countryRs.getString("country_name"),
                        addressRs.getString("address_street_name"),
                        addressRs.getString("address_street_number"),
                        roleRs.getString("role_name")
                ));
            }
        } catch (SQLException e) { e.printStackTrace(); }

        return users;
    }

    public int deleteUser(Request request) {
        String email = request.params("email");
        ResultSet userRs = Resources.getDB().getData("SELECT * FROM user WHERE user_email = " + "\"" + email + "\"");
        int userId = -1;
        int isEverythingDeleted = -1;

        try {
            userId = userRs.getInt("user_id");
        } catch (SQLException error) {
            error.printStackTrace();
        }

        // Delete user
        int isUserDeleted = Resources.getDB().removeData("DELETE FROM user WHERE user_email = " + "\"" + email + "\"");

        // Try to delete address
        if (isUserDeleted > 0) {
            int addressId = -1;

            try {
                addressId = userRs.getInt("user_address_id_fk");
            } catch (SQLException error) {
                error.printStackTrace();
            }

            int isAddressDeleted = Resources.getDB().removeData("DELETE FROM address WHERE address_id = " + "\"" + addressId + "\"");

            // Try to delete jobs
            if (isAddressDeleted > 0) {
                isEverythingDeleted = Resources.getDB().removeData("DELETE FROM jobs WHERE job_user_id = " + "\"" + userId + "\"");
            }
        }

        return isEverythingDeleted;
    }
}

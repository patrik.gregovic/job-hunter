package Root.db.beanManager;

import Root.Resources;
import Root.db.beans.PreferredSearch;
import spark.Request;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PreferredSearchManager extends Manager {

    public PreferredSearch getPreferredSearch(Request request) {
        PreferredSearch preferredSearch = null;
        int userId = Integer.parseInt(request.params("userId"));

        // Get preferred search by id
        ResultSet preferredSearchRS =
                fetch("SELECT * FROM preferred_search WHERE preferred_search_id=" + "\"" + userId + "\"");

        ResultSet provinceRS = null;

        try {
            provinceRS =
                    fetch("SELECT province_code FROM province WHERE province_id="
                            + "\""
                            + preferredSearchRS.getString("preferred_search_province_id_fk)"
                            + "\"")
                    );
        } catch (SQLException error) {
            error.printStackTrace();
        }

        try {
            preferredSearch = new PreferredSearch(
                    preferredSearchRS.getInt("preferred_search_id"),
                    preferredSearchRS.getString("preferred_search_preferred_job_type"),
                    preferredSearchRS.getString(provinceRS.getString("province_code")),
                    preferredSearchRS.getInt("preferred_search_user_id")
            );
        } catch (SQLException error) {
            error.printStackTrace();
        }

        return preferredSearch;
    }

    public boolean setPreferredSearch(Request request) {
        String provinceName = request.params("province");
        String jobType = request.params("jobType");
        String userId = request.params("userId");
        int isSuccess = -1;

        // Try to fetch preferred search of the user
        ResultSet preferredSearchRS = fetch(
                "SELECT * FROM preferred_search WHERE preferred_search_user_id = "
                        + "\"" + userId + "\"");

        // Get province ID RS
        ResultSet provinceRS =
                fetch("SELECT province_id FROM province WHERE province_code=" + "\"" + provinceName + "\"");

        try {
            // If preferred search exists, update it, else create it
            if (preferredSearchRS.next()) {
                isSuccess = Resources.getDB().setData(
                        "UPDATE preferred_search SET " +
                                "preferred_search_preferred_job_type=?, " +
                                "preferred_search_province_id_fk=? " +
                                "WHERE preferred_search_user_id=?",
                        new ArrayList<>() {{
                            add(jobType);
                            add(provinceRS.getString("province_id"));
                            add(userId);
                        }}
                );
            } else {
                isSuccess = createPreferredSearch(request, provinceRS);
            }
        } catch (SQLException error) {
            System.out.println("==================failed here===================");
            error.printStackTrace();
        }

        return isSuccess > 0;
    }

    private int createPreferredSearch(Request request, ResultSet provinceRS) throws SQLException {
        int isSuccess = -1;

        System.out.println("=============province============ " + provinceRS.getString("province_id"));

        try {
            isSuccess = Resources.getDB().setData(
                    "INSERT INTO preferred_search (" +
                            "preferred_search_preferred_job_type, " +
                            "preferred_search_province_id_fk, " +
                            "preferred_search_user_id) " +
                            "VALUES (?, ?, ?) ",
                    new ArrayList<>() {{
                        add(request.params("jobType"));
                        add(provinceRS.getString("province_id"));
                        add(request.params("userId"));
                    }}
            );
        } catch (SQLException error) {
            System.out.println("==================failed in province===================");
            error.printStackTrace();
        }

        return isSuccess;
    }

    @Override
    protected <T> T post(String query) {
        return null;
    }

    @Override
    protected <T> T put(String query) {
        return null;
    }

    @Override
    protected <T> T fetch(String query) {
        return (T) Resources.getDB().getData(query);
    }

    @Override
    protected <T> T remove(String query) {
        return null;
    }
}

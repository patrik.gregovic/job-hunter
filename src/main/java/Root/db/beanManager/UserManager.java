package Root.db.beanManager;

import Root.Resources;
import Root.db.beans.User;
import Root.util.StringParser;
import spark.Request;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class UserManager extends Manager {

    public boolean login(String id, String password) {
        return this.authenticate(id, password);
    }

    public boolean register(Request request) throws SQLException {
        ResultSet rs = Resources.getDB().getData("SELECT country_id from country WHERE country_abv=" + "\"" + request.params("country") + "\"");

        int countryID = rs.getInt("country_id");

        Resources.getDB().setData(
                "INSERT INTO address (" +
                        "address_city, " +
                        "address_country_id_fk, " +
                        "address_street_name, " +
                        "address_street_number) " +
                        "VALUES (?, ?, ?, ?)",
                new ArrayList<>() {{
                    add(StringParser.sanitize(request.params("city")));
                    add(StringParser.sanitize(String.valueOf(countryID)));
                    add(StringParser.sanitize(request.params("streetName")));
                    add(StringParser.sanitize(request.params("streetNumber")));
                }}
        );

        String addressID = Resources.getDB().getData("SELECT address_id FROM address WHERE address_id=(SELECT MAX(address_id) FROM address);").getString("address_id");

        int isSuccess = Resources.getDB().setData(
                "INSERT INTO user (" +
                        "user_first_name, " +
                        "user_last_name, " +
                        "user_email, " +
                        "user_password, " +
                        "user_address_id_fk) " +
                        "VALUES (?, ?, ?, ?, ?)",
                new ArrayList<>() {{
                    add(StringParser.sanitize(request.params("fname")));
                    add(StringParser.sanitize(request.params("lname")));
                    add(StringParser.sanitize(request.params("email")));
                    add(StringParser.sanitize(request.params("password")));
                    add(StringParser.sanitize(String.valueOf(addressID)));
                }}
        );

        return isSuccess > 0;
    }

    @Override
    protected <T> T post(String query) {
        return null;
    }

    @Override
    protected <T> T put(String query) {
        return null;
    }

    @Override
    protected <T> T fetch(String query) {
        return null;
    }

    @Override
    protected <T> T remove(String query) {
        return null;
    }

    public boolean updateProfile(Request request) throws SQLException {
        //  TODO: Get ID via email
        int userID = Resources.getDB().getData("SELECT user_id FROM user WHERE user_email=" + "\"" + request.params("email") + "\"").getInt("user_id");

        //  TODO: Update country
        int countryID = Resources.getDB().getData("SELECT country_id from country WHERE country_abv=" + "\"" + request.params("country") + "\"").getInt("country_id");

        //  TODO: Update address table
        Resources.getDB().setData(
                "UPDATE address SET " +
                        "address_city=?, " +
                        "address_country_id_fk=?, " +
                        "address_street_name=?, " +
                        "address_street_number=? " +
                        "WHERE address_id=?",
                new ArrayList<>() {{
                    add(request.params("city"));
                    add(String.valueOf(countryID));
                    add(request.params("streetName"));
                    add(request.params("streetNumber"));
                    add(String.valueOf(userID));
                }}
        );

        String addressID = Resources.getDB().getData("SELECT address_id FROM address WHERE address_id=(SELECT MAX(address_id) FROM address);").getString("address_id");


        //  TODO: Update user table
        int isSuccess = Resources.getDB().setData(
                "UPDATE user SET " +
                        "user_first_name=? , " +
                        "user_last_name=?, " +
                        "user_password=?, " +
                        "user_address_id_fk=? " +
                        "WHERE user_id=?",
                new ArrayList<>() {{
                    add(request.params("fname"));
                    add(request.params("lname"));
                    add(request.params("password"));
                    add(addressID);
                    add(String.valueOf(userID));
                }}
        );

        return isSuccess > 0;
    }

    public User getProfile(Request request) throws SQLException {
        User user = new User();
        ResultSet userRs = Resources.getDB().getData("SELECT * FROM user WHERE user_email=" + "\"" + request.params("email") + "\"");
        ResultSet addressRs = Resources.getDB().getData("SELECT * FROM address WHERE address_id=" + "\"" + userRs.getString("user_address_id_fk") + "\"");
        ResultSet countryRs = Resources.getDB().getData("SELECT country_name FROM country WHERE country_id=" + "\"" + addressRs.getString("address_country_id_fk") + "\"");

        user.setId(userRs.getString("user_id"));
        user.setFName(userRs.getString("user_first_name"));
        user.setLName(userRs.getString("user_last_name"));
        user.setEmail(userRs.getString("user_email"));
        user.setCity(addressRs.getString("address_city"));
        user.setCountry(countryRs.getString("country_name"));
        user.setStreetName(addressRs.getString("address_street_name"));
        user.setStreetNumber(addressRs.getString("address_street_number"));

        return user;
    }

    public boolean checkIfAdmin(Request request) {
        ResultSet userRs = Resources.getDB().getData("SELECT * FROM user WHERE user_email = " + "\"" + request.params("email") + "\"");
        boolean isAdmin = false;

        try {
            if(userRs.getString("user_role").equals("2")) {
                isAdmin = true;
            }
        } catch (SQLException e) { e.printStackTrace(); }

        return isAdmin;
    }
}

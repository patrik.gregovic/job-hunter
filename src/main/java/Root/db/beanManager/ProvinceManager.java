package Root.db.beanManager;

import Root.Resources;
import Root.db.beans.Province;
import spark.Request;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProvinceManager extends Manager {

    @Override
    protected <T> T post(String query) {
        return null;
    }

    @Override
    protected <T> T put(String query) {
        return null;
    }

    @Override
    protected <T> T fetch(String query) {
        return null;
    }

    @Override
    protected <T> T remove(String query) {
        return null;
    }

    public List<Province> getProvince(Request request) throws SQLException {
        List<Province> provinces = new ArrayList<>();
        ResultSet rs = Resources.getDB().getData("SELECT * FROM province WHERE province_job_website_id = " + "\"" + request.params("jobSite") + "\"");

        //  TODO: Fix exception throwing all over the place
        while (rs.next()) {
            provinces.add(new Province(rs.getString("province_code")));
        }

        return provinces;
    }
}

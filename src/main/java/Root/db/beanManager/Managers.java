package Root.db.beanManager;

import java.util.HashMap;

public enum Managers {
    USER,
    JOB,
    PREFERRED_SEARCH,
    PROVINCE,
    COUNTRY,
    ADMIN;

    public static HashMap<Managers, Manager> createManagers() {
        HashMap<Managers, Manager> managers = new HashMap();
        managers.put(Managers.USER, new UserManager());
        managers.put(Managers.JOB, new JobManager());
        managers.put(Managers.PREFERRED_SEARCH, new PreferredSearchManager());
        managers.put(Managers.PROVINCE, new ProvinceManager());
        managers.put(Managers.COUNTRY, new CountryManager());
        managers.put(Managers.ADMIN, new AdminManager());
        return managers;
    }
}

package Root.db.beanManager;

import Root.Resources;
import Root.db.beans.Country;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CountryManager extends Manager {
    @Override
    protected <T> T post(String query) {
        return null;
    }

    @Override
    protected <T> T put(String query) {
        return null;
    }

    @Override
    protected <T> T fetch(String query) {
        return null;
    }

    @Override
    protected <T> T remove(String query) {
        return null;
    }

    public List<Country> getAllCountries() throws SQLException {
        List<Country> countries = new ArrayList<>();

        ResultSet rs = Resources.getDB().getData("SELECT * FROM country;");

        while (rs.next()) {
            countries.add(new Country(
                    rs.getString("country_name"),
                    rs.getString("country_abv")
            ));
        }

        return countries;
    }
}

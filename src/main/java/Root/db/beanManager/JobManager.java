package Root.db.beanManager;

import Root.Resources;
import Root.db.beans.JobPosting;
import Root.util.StringParser;
import spark.Request;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JobManager extends Manager {

    // TODO Verify employer id type
    public boolean saveJob(Request request) throws SQLException {
        int userID = Resources.getDB().getData("SELECT user_id FROM user WHERE user_email=" + "\"" + request.params("email") + "\"").getInt("user_id");
        int employerId = findEmployer(request.params("employerName"));

        // If no employer found create it
        if (employerId == -1) {
            employerId = saveEmployer(request.params("employerName"), request.params("employerLocation"));
        }

        // Todo fix this
        int finalEmployerId = employerId;

        int jobId = Resources.getDB().setData(
                "INSERT INTO jobs (" +
                        "job_employer_id_fk, " +
                        "job_url, " +
                        "job_location, " +
                        "job_expires, " +
                        "job_position, " +
                        "job_user_id) " +
                        "VALUES (?, ?, ?, ?, ?, ?)",
                new ArrayList<>() {{
                    add(StringParser.sanitize(String.valueOf(finalEmployerId)));
                    add(StringParser.sanitize(request.params("jobUrl")));
                    add(StringParser.sanitize(String.valueOf(request.params("jobLocation"))));
                    add(StringParser.sanitize(request.params("jobExpires")));
                    add(StringParser.sanitize(request.params("jobPosition")));
                    add(StringParser.sanitize(String.valueOf(userID)));
                }}
        );

        return jobId > 0;
    }

    public int saveEmployer(String employerName, String employerLocation) {
        return Resources.getDB().setData(
                "INSERT INTO employer (" +
                        "employer_name, " +
                        "employer_location) " +
                        "VALUES (?, ?) ",
                new ArrayList<>() {{
                    add(employerName);
                    add(employerLocation);
                }}
        );
    }

    // Find employer by employer name
    public int findEmployer(String employerName) {
        ResultSet employerRS = Resources.getDB().getData("SELECT employer_id from employer WHERE employer_name=" + "\"" + employerName + "\"");
        int employerId = -1;

        try {
            employerId = employerRS.getInt("employer_id");
        } catch (SQLException error) {
            error.printStackTrace();
        }

        return employerId;
    }

    // Remove job from database by id
    public boolean removeJob(Request request) {
        int jobId = Integer.parseInt(request.params("jobId"));
        System.out.println("===========================" + jobId);
        int isSuccess = Resources.getDB().removeData("DELETE FROM jobs WHERE job_id=" + "\"" + jobId + "\"");

        return isSuccess == 1;
    }

    @Override
    protected <T> T post(String query) {
        return null;
    }

    @Override
    protected <T> T put(String query) {
        return null;
    }

    @Override
    protected <T> T fetch(String query) {
        return (T) Resources.getDB().getData(query);
    }

    @Override
    protected <T> T remove(String query) {
        return null;
    }

    public List<JobPosting> getSavedJobs(Request request) throws SQLException {
        List<JobPosting> jobPostings = new ArrayList<>();
        int userID = Resources.getDB().getData("SELECT user_id FROM user WHERE user_email=" + "\"" + request.params("email") + "\"").getInt("user_id");
        ResultSet rs = Resources.getDB().getData("SELECT * FROM jobs WHERE job_user_id=" + "\"" + userID + "\"");

        while (rs.next()) {
            jobPostings.add(
                    new JobPosting(
                            rs.getInt("job_id"),
                            rs.getString("job_url"),
                            rs.getString("job_position"),
                            rs.getString("job_location"),
                            rs.getInt("job_expires"),
                            rs.getString("job_user_id")
                    )
            );
        }

        return jobPostings;
    }
}

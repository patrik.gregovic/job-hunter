package Root.db.routes;

import Root.Resources;
import Root.db.beanManager.JobManager;
import Root.db.beanManager.Manager;
import Root.db.beanManager.Managers;
import Root.db.beans.JobPosting;
import Root.scrapper.SearchConfig;
import Root.util.StringParser;

import java.util.HashMap;
import java.util.List;

import static spark.Spark.*;

public class JobRoutes {

    public static void initJobRoutes(HashMap<Managers, Manager> managers) {
        /* Scrape request from user */
        get("/search/:province/:profession/:jobSite", (request, response) -> {
            List<JobPosting> list = Resources.getJobService().getJobs(new SearchConfig(
                    Integer.parseInt(request.params(":province")),
                    StringParser.sanitize(request.params(":profession")),
                    SearchConfig.Search.valueOf(request.params(":jobSite"))
            ));
            return Resources.getGson().toJson(list);
        });

        /* Save job */
        post("/jobs/:email/:employerName/:employerLocation/:jobUrl/:jobLocation/:jobExpires/:jobPosition", (request, response) -> {
            boolean isSuccess = ((JobManager) managers.get(Managers.JOB)).saveJob(request);
            return Resources.getGson().toJson(isSuccess);
        });

        /* Remove a job from favorites */
        delete("/jobs/delete/:jobId", (request, response) -> {
            boolean isSuccess = ((JobManager) managers.get(Managers.JOB)).removeJob(request);
            return Resources.getGson().toJson(isSuccess);
        });

        /* Get saved jobs */
        get("/jobs/:email", (request, response) -> {
            List<JobPosting> jobPostings = ((JobManager) managers.get(Managers.JOB)).getSavedJobs(request);
            return Resources.getGson().toJson(jobPostings);
        });
    }
}

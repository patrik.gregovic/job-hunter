package Root.db.routes;

import Root.Resources;
import Root.db.beanManager.Manager;
import Root.db.beanManager.Managers;
import Root.db.beanManager.ProvinceManager;
import Root.db.beans.Province;

import java.util.HashMap;
import java.util.List;

import static spark.Spark.get;

public class ProvincesRoutes {

    public static void initProvincesRoutes(HashMap<Managers, Manager> managers) {
        // Get provinces - 1 or 2 = 1 is for Posao.hr, 2 is for Mojposao.hr
        get("/provinces/:jobSite", (request, response) -> {
            List<Province> provinces = ((ProvinceManager) managers.get(Managers.PROVINCE)).getProvince(request);
            return Resources.getGson().toJson(provinces);
        });
    }
}

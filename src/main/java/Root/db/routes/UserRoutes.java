package Root.db.routes;

import Root.Resources;
import Root.db.beanManager.Manager;
import Root.db.beanManager.Managers;
import Root.db.beanManager.UserManager;
import Root.db.beans.User;

import java.util.HashMap;

import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.put;

public class UserRoutes {

    public static void initUserRoutes(HashMap<Managers, Manager> managers) {
        /* Attempt to login a user */
        get("/login/:email/:password", (request, response) -> {
            boolean isSuccess = ((UserManager) managers.get(Managers.USER)).login(
                    request.params("email"),
                    request.params("password")
            );
            return Resources.getGson().toJson(isSuccess);
        });

        /* Check if logged in user has admin role */
        get("/login/:email", (request, response) -> {
            boolean isAdmin = ((UserManager) managers.get(Managers.USER)).checkIfAdmin(request);
            return Resources.getGson().toJson(isAdmin);
        });

        /* Get user by email */
        get("/profile/:email", (request, response) -> {
            User user = ((UserManager) managers.get(Managers.USER)).getProfile(request);
            return Resources.getGson().toJson(user);
        });

        /* Attempt to register new user */
        post("/register/:fname/:lname/:email/:streetName/:streetNumber/:city/:country/:password", (request, response) -> {
            boolean isSuccess = ((UserManager) managers.get(Managers.USER)).register(request);
            return Resources.getGson().toJson(isSuccess);
        });

        /* Attempt to update user's data */
        put("/profile/:fname/:lname/:email/:streetName/:streetNumber/:city/:country/:password", (request, response) -> {
            boolean isSuccess = ((UserManager) managers.get(Managers.USER)).updateProfile(request);
            return Resources.getGson().toJson(isSuccess);
        });
    }
}

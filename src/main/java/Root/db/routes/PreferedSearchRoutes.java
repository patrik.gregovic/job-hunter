package Root.db.routes;

import Root.Resources;
import Root.db.beanManager.Manager;
import Root.db.beanManager.Managers;
import Root.db.beanManager.PreferredSearchManager;
import Root.db.beans.PreferredSearch;

import java.util.HashMap;

import static spark.Spark.get;
import static spark.Spark.post;

public class PreferedSearchRoutes {

    public static void initPreferedSearchRoutes(HashMap<Managers, Manager> managers) {
        // Get users preferred search
        get("/preferredSearch/:userId/", (request, response) -> {
            PreferredSearch preferredSearch = ((PreferredSearchManager) managers.get(Managers.PREFERRED_SEARCH))
                    .getPreferredSearch(request);
            return Resources.getGson().toJson(preferredSearch);
        });

        // Set users preferred search
        post("/preferredSearch/:userId/:jobType/:province", (request, response) -> {
            return Resources.getGson().toJson(((PreferredSearchManager) managers.get(Managers.PREFERRED_SEARCH)).setPreferredSearch(request));
        });
    }
}

package Root.db.routes;

import Root.Resources;
import Root.db.beanManager.AdminManager;
import Root.db.beanManager.Manager;
import Root.db.beanManager.Managers;
import Root.db.beans.User;

import java.util.HashMap;
import java.util.List;

import static spark.Spark.delete;
import static spark.Spark.get;

public class AdminRoutes {

    public static void init(HashMap<Managers, Manager> managers) {
        get("/admin/getAll/:email", (request, response) -> {
            List<User> users = ((AdminManager) managers.get(Managers.ADMIN)).getAllUsers(request);
            return Resources.getGson().toJson(users);
        });

        delete("/admin/remove/:email", (request, response) -> {
            int isSuccess = ((AdminManager) managers.get(Managers.ADMIN)).deleteUser(request);
            return isSuccess > 0;
        });
    }
}

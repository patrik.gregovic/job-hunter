package Root.db.routes;

import Root.Resources;
import Root.db.beanManager.CountryManager;
import Root.db.beanManager.Manager;
import Root.db.beanManager.Managers;
import Root.db.beans.Country;

import java.util.HashMap;
import java.util.List;

import static spark.Spark.get;

public class CountriesRoutes {

    /**
     *
     * @param managers
     */
    public static void initCountriesRoutes(HashMap<Managers, Manager> managers) {
        get("/countries", (request, response) -> {
            List<Country> countries = ((CountryManager) managers.get(Managers.COUNTRY)).getAllCountries();
            return Resources.getGson().toJson(countries);
        });
    }
}

package Root.scrapper;

import Root.db.beans.JobPosting;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.jsoup.select.NodeVisitor;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

class MojPosaoScrapper extends WebScrapper implements Scrapper {
    private final String URL = "https://www.moj-posao.net/Pretraga-Poslova/";
    private final MojPosaoProvinces provinces = new MojPosaoProvinces();

    @Override
    public List<JobPosting> scrape(SearchConfig searchConfig) {
        List<String> data = new ArrayList<>();

        try {
            Element current = null;
            Elements pagination = null;
            do {

                Document doc = Jsoup.connect(this.buildURL(searchConfig)).get();
                Element searchList = doc.getElementsByClass("searchlist").first();

                if (!searchList.child(0).attr("class").contains("grey text-subtitle")) {
                    this.extractData(searchList, data);
                }

                current = doc.getElementsByClass("active").last();
                pagination = doc.getElementsByClass("pagination");

                this.currPage++;
            } while (!current.nextElementSibling().hasClass("unavailable") &&
                    !pagination.toString().equals(""));

        } catch (Exception e) {
            e.printStackTrace();
        }

        this.currPage = 1;

        return this.createJobList(data);
    }

    private String buildURL(SearchConfig searchConfig) {
        return this.URL +
                "?searchWord=" + searchConfig.profession +
                "&keyword=" + searchConfig.profession +
                "&job_title=&job_title_id=&area=" + this.provinces.get(searchConfig.province) +
                "&category=&page=" + this.currPage;
    }

    private void extractData(Element searchlist, List<String> data) {
        Elements jobList = searchlist.getElementsByClass("job-data");

        jobList.traverse(new NodeVisitor() {

            @Override
            public void head(Node node, int i) {
                switch (node.attr("class")) {
                    case "job-title":
                        data.add(node.childNode(1).attr("href"));
                        data.add(((Element) node.childNode(1)).text());
                        break;
                    case "job-location":
                        data.add(((Element) node).text());
                        break;
                    case "job-company":
                        if (node.childNode(0) instanceof TextNode) {
                            data.add(node.childNode(0).attr("#text"));
                        } else {
                            data.add(((Element) node.childNode(0)).text());
                        }
                        break;
                    case "deadline":
                        data.add(node.childNode(1).attr("datetime"));
                        break;
                }
            }

            @Override
            public void tail(Node node, int i) {

            }
        });
    }

    @Override
    protected JobPosting createJob(List<String> tempList) {
        return new JobPosting(
                tempList.get(0),
                tempList.get(1),
                tempList.get(3),
                this.toDate(tempList.get(4)),
                tempList.get(2)
        );
    }

    private long toDate(String date) {
        String[] timeParts = date.split("-| |:");
        return new GregorianCalendar(
                Integer.parseInt(timeParts[0]),
                Integer.parseInt(timeParts[1]) == 0 ? 0 : Integer.parseInt(timeParts[1]) - 1,
                Integer.parseInt(timeParts[2]),
                Integer.parseInt(timeParts[3]),
                Integer.parseInt(timeParts[4])
        ).getTimeInMillis();
    }
}

package Root.scrapper;

public class SearchConfig {
    public int province;
    public String profession;
    public Search searchOn;

    public SearchConfig(int province, String profession, Search searchOn) {
        this.province = province;
        this.profession = profession;
        this.searchOn = searchOn;
    }

    public enum Search {
        ALL(0),
        POSAO_HR(1),
        MOJPOSAO_HR(2);

        Search(int i) {}
    }
}

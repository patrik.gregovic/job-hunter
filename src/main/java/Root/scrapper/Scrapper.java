package Root.scrapper;

import Root.db.beans.JobPosting;

import java.util.List;

interface Scrapper {
    List<JobPosting> scrape(SearchConfig searchConfig);
}

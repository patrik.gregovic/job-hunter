package Root.scrapper;

import Root.db.beans.JobPosting;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.jsoup.select.NodeVisitor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


class PosaoHrScrapper extends WebScrapper implements Scrapper {
    private final String URL = "https://www.posao.hr/poslovi/izraz/";
    private final PosaoHrProvinces provinces = new PosaoHrProvinces();


    public List<JobPosting> scrape(SearchConfig searchConfig) {
        List<String> data = new ArrayList<>();

        try {
            Element current = null;
            do {
                Document doc = Jsoup.connect(this.buildURL(searchConfig)).get();
                Elements jobPostingsHTML = doc.getElementsByClass("list box").select("div > a");

                this.extractData(jobPostingsHTML, data);

                current = doc.getElementsByClass("current").first();
                this.currPage++;
            } while (current != null && current.hasText());
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.currPage = 1;

        return this.createJobList(data);
    }

    private String buildURL(SearchConfig searchConfig) {
        String zupanija = "/zupanija/" + provinces.get(searchConfig.province);
        return this.URL + searchConfig.profession +
                (zupanija.equals("/zupanija/") ? "" : zupanija) +
                "/stranica/" + this.currPage;
    }


    private void extractData(Elements postingContainer, List<String> data) {

        postingContainer.traverse(new NodeVisitor() {
            @Override
            public void head(Node node, int i) {

                if (node.nodeName().equals("a") && node.childNode(0).attr("class").contains("search_grid")) {
                    data.add(node.attr("href"));
                }

                if (node.attr("class").contains("title") && !node.parent().parent().attr("class").contains("logo")) {
                    data.add(((Element) node).text());
                }

                switch (node.attr("class")) {
                    case "location":
                    case "res_date_time_comp location":
                    case "deadline":
                    case "res_date_time_comp deadline":
                    case "res_date_time_comp employer company":
                    case "employer res_date_time_comp company":
                    case "res_date_time_comp deadline expires-today":
                        data.add(((Element) node).text());
                        break;
                }
            }

            @Override
            public void tail(Node node, int i) {
            }
        });
    }

    @Override
    protected JobPosting createJob(List<String> tempList) {
        return new JobPosting(
                tempList.get(0),
                tempList.get(1),
                tempList.get(2),
                this.toDate(tempList.get(3)),
                tempList.get(4)
        );
    }

    private long toDate(String date) {
        String dayInUnixEpoch = date.replaceAll("\\D+", "");

        if (dayInUnixEpoch.equals("")) {
            return -1;
        } else {
            return new Date().getTime() + Integer.parseInt(dayInUnixEpoch) * (86400 * 1000);
        }
    }
}

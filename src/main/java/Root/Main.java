package Root;

import Root.db.beanManager.*;
import Root.db.routes.*;
import org.apache.log4j.BasicConfigurator;

import java.util.HashMap;

import static spark.Spark.*;

public class Main {

    public static void main(String[] args) {
        initSparkConfig();
        initRoutes();
    }

    private static void initRoutes() {
        HashMap<Managers, Manager> managers = Managers.createManagers();
        UserRoutes.initUserRoutes(managers);
        CountriesRoutes.initCountriesRoutes(managers);
        JobRoutes.initJobRoutes(managers);
        PreferedSearchRoutes.initPreferedSearchRoutes(managers);
        ProvincesRoutes.initProvincesRoutes(managers);
        AdminRoutes.init(managers);
    }

    public static void initSparkConfig() {
        BasicConfigurator.configure();
        port(3000); 
        threadPool(10);
    }
}
